def vowel_swapper(string):
    # ==============
    # Your code here
str = "aAa eEe iIi oOo uUu"
x = str.replace("aAa", "a/\a"), str.replace("eEe", "e3e"), str.replace("iIi", "i!i"), str.replace("oOo", "o000o"), str.replace("uUu", "u\/u")
print(x)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
