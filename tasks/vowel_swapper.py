def vowel_swapper(string):
    # ==============
    # Your code here
str = "aA eE iI oO uU"
x = str.replace("aA", "44"), str.replace("eE", "33"), str.replace("iI", "!!"), str.replace("oO", "ooo000"), str.replace("uU", "|_||_|")
print(x)
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
